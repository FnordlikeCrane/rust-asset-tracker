// ~/util.rs
//-----------------------------------------------

use crate::coingecko::*;
use std::{collections::HashMap};


//-----------------------------------------------
// CONSTANTS

pub const CONFIG_PATH: &'static str = "Config.conf";


//-----------------------------------------------
// Simple Util Functions

#[allow(dead_code)]
pub fn str_is_digits(s: &str) -> bool {
	let chars = s.as_bytes();
	for byte in chars.iter() {
		let ch = *byte as char;
		let is_digit = ch.to_digit(10);
		match is_digit {
			Some(_digit) => {
				continue;
			},
			None => {
				return false;
			}
		}
	}

	true
}

#[allow(dead_code)]
pub fn string_to_u32(s: &String) -> u32 {
	s.chars().map(|c| c.to_digit(10).unwrap()).sum::<u32>()
}

#[allow(dead_code)]
pub async fn market_data_for_asset_names(names: &Vec<String>) -> (HashMap<String, HistoricalMarketChart>, Vec<String>) {
	let mut market_data = HashMap::new();
	let mut ids = Vec::new();
	for name in names {
		if name == "" {
			ids.push(String::from(""));
		} else {
			let id = match cg_search_asset_id(name).await {
				Some(id) => id,
				None => String::from(""),
			};
			if id != "" {
				let market_chart = match cg_get_market_chart(&id).await {
					Some(mc) => mc,
					None => continue,
				};
				market_data.insert(id.clone(), market_chart);
			}
			ids.push(id);
		}
	}
	(market_data, ids)
}
