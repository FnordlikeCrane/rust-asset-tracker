// ~/coingecko.rs
//-----------------------------------------------

use serde::{Deserialize, Serialize};
use serde_json::Value;
use reqwest::StatusCode;
use async_compat::CompatExt;


//-----------------------------------------------
// Structs

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct SearchedCoin {
    id: String,
    name: String,
	symbol: String,
	market_cap_rank: Option<u32>,
	thumb: String,
	large: String,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct AssetValues {
	usd: Option<f64>
}

#[derive(Serialize, Deserialize, Clone)]
pub struct MarketData {
	current_price: AssetValues
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Asset {
    id: String,
	symbol: String,
	name: String,
	// asset_platform_id: Option<Value>,
	// platforms: Value,
	// block_time_in_minutes: i64,
	// hashing_algorithm: Option<String>,
	// categories: Vec<Value>,
	// public_notice: Value,
	// additional_notices: Vec<Value>,
	// localization: Value,
	// description: Value,
	// links: Value,
	// image: Value,
	// country_origin: String,
	// genesis_date: String,
	// sentiment_votes_up_percentage: Option<f32>,
	// sentiment_votes_down_percentage: Option<f32>,
	// market_cap_rank: Option<u32>,
	// coingecko_rank: Option<u32>,
	// coingecko_score: Option<f32>,
	// developer_score: Option<f32>,
	// community_score: Option<f32>,
	// liquidity_score: Option<f32>,
	// public_interest_score: Option<f32>,
	market_data: MarketData,
	// community_data: Value,
	// developer_data: Value,
	// public_interest_stats: Value,
	// status_updates: Vec<Value>,
	// last_updated: String,
	// tickers: Vec<Value>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SearchPayload {
	coins: Vec<SearchedCoin>,
	exchanges: Vec<Value>,
	icos: Vec<Value>,
	categories: Vec<Value>,
	nfts: Vec<Value>
}

#[derive(Deserialize, Debug, Clone)]
pub struct MarketChartPayload {
	prices: Vec<Vec<Option<f64>>>,
	// market_caps: Vec<Vec<Option<f64>>>,
	total_volumes: Vec<Vec<Option<f64>>>,
}

#[derive(Serialize, Debug, Clone)]
pub struct HistoricalMarketChart {
	pub prices: Vec<f64>,
	pub total_volumes: Vec<f64>,
}
impl HistoricalMarketChart {
	pub fn new(prices: Vec<f64>, total_volumes: Vec<f64>) -> HistoricalMarketChart {
		HistoricalMarketChart{prices, total_volumes}
	}
}


//-----------------------------------------------
// CoinGecko Functions

fn payload_helper(v: &Vec<Vec<Option<f64>>>) -> Vec<f64> {
	let mut new_v: Vec<f64> = Vec::new();
	for n in v.iter() {
		match n[1] {
			Some(n) => new_v.push(n),
			None => continue
		}
	}
	new_v = new_v.into_iter().rev().collect();
	new_v
}

pub async fn cg_get_market_chart(id: &String) -> Option<HistoricalMarketChart> {
	let search_url = format!("https://api.coingecko.com/api/v3/coins/{}/market_chart?vs_currency=usd&days=11430", id);
	
	let resp = reqwest::get(search_url.as_str()).compat().await;
	if resp.is_err() {
		eprintln!("ERROR: Error searching asset \"{id}\" with url \"{search_url}\"");
		return None;
	}
	let resp = resp.unwrap();
	match resp.status() {
		StatusCode::OK => {
			let payload: MarketChartPayload = resp.json().await.expect(format!("Could not deserialize data from coingecko with id \"{id}\"").as_str());
			let prices = payload_helper(&payload.prices);
			let total_volumes = payload_helper(&payload.total_volumes);

			let market_chart = HistoricalMarketChart::new(prices,total_volumes);
			Some(market_chart)
		},
		_status => {
			eprintln!("ERROR: No valid response code from url \"{search_url}\"");
			None
		}
	}
}

pub async fn cg_search_asset_id(searching: &String) -> Option<String> {
	let no_space_searching = str::replace(searching.as_str(), " ", "-"); 
	let url_encoded_searching = str::replace(searching.as_str(), " ", "%20");
	// if debug_on() >= 4 {
	// 	println!("Searching for: {}", no_space_searching);
	// }

	let search_url = format!("https://api.coingecko.com/api/v3/search?query={}", url_encoded_searching).to_string();

	let resp = reqwest::get(search_url.as_str()).compat().await;
	if resp.is_err() {
		eprintln!("ERROR: Error searching asset \"{searching}\" with url \"{search_url}\"");
		return None;
	}
	let resp = resp.unwrap();
	match resp.status() {
		StatusCode::OK => {
			let payload: SearchPayload = resp.json().await.unwrap();
			let coins: Vec<SearchedCoin> = payload.clone().coins;
			for coin in coins.iter() {
				let check_id = coin.clone().id;
				if coin.id.to_ascii_lowercase() == no_space_searching.to_ascii_lowercase() {
					return Some(check_id);
				} else if coin.name.as_str().to_ascii_lowercase() == no_space_searching.as_str().to_ascii_lowercase() {
					return Some(check_id);
				} else if coin.symbol.as_str().to_ascii_lowercase() == no_space_searching.as_str().to_ascii_lowercase() {
					return Some(check_id);
				}
			}
			None
		},
		_status => {
			None
		},
	}
}
