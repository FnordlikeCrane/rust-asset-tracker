// ~/write_spreadsheet.rs
//-----------------------------------------------

use chrono::{DateTime, Utc, NaiveDateTime};
use std::collections::HashMap;

use crate::config::*;
use crate::spreadsheet_types::{ApiCall, DesiredData};
use xlsxwriter::*;


//-----------------------------------------------
// Spreadsheet Functions

pub fn make_default_sheet(path: &String, sheet_name: &String) -> Result<(), XlsxError> {
	let book = Workbook::new(path.as_str());
	let f_bold = book.add_format().set_bold();

	let mut assets_sheet =  book.add_worksheet(Some(sheet_name.as_str())).expect(format!("Could not add sheet to file: \"{}\"", path).as_str());
	
	// Format Column Widths
	assets_sheet.set_column(0, 4, 12.4, None)?;
	assets_sheet.set_column(4, 5, 24.0, None)?;
	
	// Format Column Headers
	assets_sheet.write_string(0,0, "Symbol", Some(&f_bold))?;
	assets_sheet.write_string(0,1, "Asset", Some(&f_bold))?;
	assets_sheet.write_string(0,2, "Asset Class", Some(&f_bold))?;
	assets_sheet.write_string(0,3, "API Source", Some(&f_bold))?;
	assets_sheet.write_string(0,4, "Desired Data", Some(&f_bold))?;

	// Write Data that's to be tracked
	// BTC
	assets_sheet.write_string(1,0, "BTC", None)?;
	assets_sheet.write_string(1, 1, "Bitcoin", None)?;
	assets_sheet.write_string(1, 2, "Crypto Currency", None)?;
	assets_sheet.write_string(1, 3, "Coingecko", None)?;
	assets_sheet.write_string(1, 4, "price, volume", None)?;

	// Gold
	assets_sheet.write_string(2,0, "gld", None)?;
	assets_sheet.write_string(2, 1, "Gold", None)?;
	assets_sheet.write_string(2, 2, "Commodity", None)?;
	assets_sheet.write_string(2, 3, "Yahoo", None)?;
	assets_sheet.write_string(2, 4, "price, volume", None)?;

	// btc-perp
	assets_sheet.write_string(3,0, "btc-perp", None)?;
	assets_sheet.write_string(3, 1, "Bitcoin", None)?;
	assets_sheet.write_string(3, 2, "cryptoperp", None)?;
	assets_sheet.write_string(3, 3, "FTX", None)?;
	assets_sheet.write_string(3, 4, "price, volume, funding rate, open interest", None)?;

	// AAVE-0624
	assets_sheet.write_string(4,0, "AAVE-0624", None)?;
	assets_sheet.write_string(4, 1, "Aave", None)?;
	assets_sheet.write_string(4, 2, "cryptofuture", None)?;
	assets_sheet.write_string(4, 3, "FTX", None)?;
	assets_sheet.write_string(4, 4, "price, volume, open interest", None)?;

	// ^SPX
	assets_sheet.write_string(5,0, "^SPX", None)?;
	assets_sheet.write_string(5, 1, "S&P 500", None)?;
	assets_sheet.write_string(5, 2, "equity", None)?;
	assets_sheet.write_string(5, 3, "Yahoo", None)?;
	assets_sheet.write_string(5, 4, "price, volume", None)?;

	Ok(())
}

pub fn write_market_history(book: &Workbook, names: &Vec<String>, call_results: &HashMap<String, Option<ApiCall>>, config: &Config) {
	// Declare formats for writing to excel
	let f_bold = book.add_format().set_bold();
	let f_curr = book.add_format().set_num_format("[$$-409]#,##0.00;[RED]-[$$-409]#,##0.00").set_shrink();
	let f_vol = book.add_format().set_text_wrap();
	
	let mut log: Vec<(String, (usize, usize))> = Vec::new();

	// Parse all calls to see what data was retrieved
	let (mut want_vol, mut want_price, mut want_fund, mut want_oi): (bool, bool, bool, bool) = (false, false, false, false);
	for call in call_results.values() {
		let desires = match call {
			Some(c)=> c.to_owned().desired_data,
			None => continue,
		};
		if desires.contains(&DesiredData::Volume) {
			want_vol = true;
		}
		if desires.contains(&DesiredData::Price) {
			want_price = true;
		}
		if desires.contains(&DesiredData::FundingRate) {
			want_fund = true;
		}
		if desires.contains(&DesiredData::OpenInterest) {
			want_oi = true;
		}
	}
	
	let date_col_width = match config.formatting_config.date_col_width <= 0.0 {
		true => FormattingConfig::default().date_col_width,
		false => config.formatting_config.date_col_width,
	};

	// Create the sheets that we need
	let mut err_offset: usize = 0;
	let mut err_row: usize = 0;
	match want_price { 
		true => {
			let mut price_sheet = book.add_worksheet(Some("Price")).unwrap();
			price_sheet.write_string(0, 0, "Date", Some(&f_bold)).unwrap();
			price_sheet.set_column(0, 0, date_col_width.clone(), None).unwrap();
			if call_results.keys().len() != 0 {
				let price_col_width = match config.formatting_config.price_col_width <= 0.0 {
					true => FormattingConfig::default().price_col_width,
					false => config.formatting_config.price_col_width,
				};
				price_sheet.set_column(1, call_results.keys().len() as u16, price_col_width, None).unwrap();
			}
			// Find the longest history of prices
			let all_prices: Vec<Option<Vec<f64>>> = call_results.values().into_iter().map(|call| match call {
				Some(c) => {
					if c.desired_data.contains(&DesiredData::Price) {
						let prices = match c.results.to_owned().price {
							Some(p) => Some(p),
							None => None,
						};
						prices
					} else { 
						None 
					}
				}, None => {
					None
				},
			}).collect();
			let mut longest_history: usize = 0;
			for prices in all_prices.iter() {
				match prices {
					Some(prices) => {
						longest_history = std::cmp::max(longest_history, prices.len());
					},
					None => continue,
				}
			}
			// Write the dates going back as far as the longest market history
			let mut now = Utc::now();
			for row in 0..longest_history {
				let today = now.date();
				price_sheet.write_string(row as WorksheetRow+1, 0, today.format("%Y-%m-%d").to_string().as_str(), None).unwrap();
				let time = now.timestamp() - 86400;
				now = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(time, 0), Utc);
			}
			// Write Prices in price sheet
			let mut col: usize = 0;
			for name in names.iter() {
				match call_results.get(name) {
					Some(call) => {
						match call {
							Some(call) => {
								if call.desired_data.contains(&DesiredData::Price) {
									let prices = match call.results.to_owned().price {
										Some(prices) => {
											prices
										},
										None => {
											log.push((String::from("Error getting price data"), (col+err_offset, err_row)));
											eprintln!("Error getting price data for \"{name}\"");
											err_offset = err_offset + 1;
											err_row = err_row + 1;
											continue;
										},
									};
									price_sheet.write_string(0, col as WorksheetCol+1, name, Some(&f_bold)).unwrap();
									for (row, price) in prices.iter().enumerate() {
										price_sheet.write_number(row as WorksheetRow+1,  col as WorksheetCol+1, price.clone(), Some(&f_curr)).unwrap();
									}
									col = col + 1;
								} else {
									continue;
								}		
							},
							None => {
								log.push((String::from("No data could be found"), (col+err_offset, err_row)));
								eprintln!("No data could be found for symbol \"{name}\"");
								err_offset = err_offset + 1;
								err_row = err_row + 1;
							},
						}
					},
					None => {
						eprintln!("No data with name {name} found in results");
					}
				};
			}
		},
		false => {},
	}
	// Write Volumes
	match want_vol {
		true => {
			let mut volume_sheet = book.add_worksheet(Some("Volume")).unwrap();
			volume_sheet.set_column(0, 0, date_col_width.clone(), None).unwrap();
			if call_results.values().len() != 0 {
				let vol_col_width = match config.formatting_config.vol_col_width <= 0.0 {
					true => FormattingConfig::default().vol_col_width,
					false => config.formatting_config.vol_col_width,
				};
				volume_sheet.set_column(1, call_results.values().len() as u16, vol_col_width, None).unwrap();
			}
			volume_sheet.write_string(0, 0, "Date", Some(&f_bold)).unwrap();
			// Find the longest history of prices
			let all_volumes: Vec<Option<Vec<f64>>> = call_results.values().into_iter().map(|call| match call {
				Some(c) => {
					if c.desired_data.contains(&DesiredData::Volume) {
						let volumes = match c.results.to_owned().price {
							Some(p) => Some(p),
							None => None,
						};
						volumes
					} else { 
						None 
					}
				}, None => {
					None
				},
			}).collect();
			let mut longest_history: usize = 0;
			for volumes in all_volumes.iter() {
				match volumes {
					Some(volumes) => {
						longest_history = std::cmp::max(longest_history, volumes.len());
					},
					None => continue,
				}
			}
			// Write the dates going back as far as the longest market history
			let mut now = Utc::now();
			for row in 0..longest_history {
				let today = now.date();
				volume_sheet.write_string(row as WorksheetRow+1, 0, today.format("%Y-%m-%d").to_string().as_str(), None).unwrap();
				let time = now.timestamp() - 86400;
				now = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(time, 0), Utc);
			}
			// Write Volumes to the volume sheet
			let mut col: usize = 0;
			for name in names.iter() {
				match call_results.get(name) {
					Some(call) => {
						match call {
							Some(call) => {
								if call.desired_data.contains(&DesiredData::Volume) {
									let volumes = match call.results.to_owned().volume {
										Some(volumes) => {
											volumes
										},
										None => {
											log.push((String::from("Error getting volume data"), (col+err_offset, err_row)));
											eprintln!("Error getting volume data for \"{name}\"");
											err_offset = err_offset + 1;
											err_row = err_row + 1;
											continue;
										},
									};
									volume_sheet.write_string(0, col as WorksheetCol+1, name, Some(&f_bold)).unwrap();
									for (row, volume) in volumes.iter().enumerate() {
										volume_sheet.write_number(row as WorksheetRow+1,  col as WorksheetCol+1, volume.clone(), Some(&f_vol)).unwrap();
									}
									col = col + 1;
								} else {
									continue;
								}		
							},
							None => {
								log.push((String::from("No data could be found"), (col+err_offset, err_row)));
								eprintln!("No data could be found for symbol \"{name}\"");
								err_offset = err_offset + 1;
								err_row = err_row + 1;
							},
						}
					},
					None => {
						eprintln!("No data with name {name} found in results");
					}
				}
			}
		},
		false => {},
	}
	// Write Funding Rates
	match want_fund {
		true => {
			let mut fund_sheet = book.add_worksheet(Some("Funding Rate")).unwrap();
			// Write Fund Rates to the Fund Rates sheet
			let mut col: usize = 0;
			for name in names.iter() {
				match call_results.get(name) {
					Some(call) => {
						match call {
							Some(call) => {
								if call.desired_data.contains(&DesiredData::FundingRate) {
									let (rates, times): (Vec<f64>, Vec<String>) = match call.results.to_owned().funding_rate {
										Some((r, t)) => {
											(r, t)
										},
										None => {
											log.push((String::from("Error getting funding rate data"), (col+err_offset, err_row)));
											eprintln!("Error funding rate data for \"{name}\"");
											err_offset = err_offset + 1;
											err_row = err_row + 1;
											continue;
										},
									};
									let wanted_calls: Vec<Option<ApiCall>> = call_results.values().into_iter().map(|call| {
										match call {
											Some(c) => {
												match c.desired_data.contains(&DesiredData::FundingRate) {
													true => { Some(c.to_owned()) }
													false => { None },
												}
											},
											None => { None },
										}
									}).collect();
									let mut length: usize = 0;
									for c in wanted_calls.iter() {
										if *c != None {
											length = length+1;
										}
									}
									
									let long_date_col_width = match config.formatting_config.long_date_col_width <= 0.0 {
										true => FormattingConfig::default().long_date_col_width,
										false => config.formatting_config.long_date_col_width,
									};
									fund_sheet.set_column(0, 0, long_date_col_width, None).unwrap();
									fund_sheet.write_string(0, 0, "Date/Time", Some(&f_bold)).unwrap();
									if length != 0 {
										let funds_col_width = match config.formatting_config.funds_col_width <= 0.0 {
											true => FormattingConfig::default().funds_col_width,
											false => config.formatting_config.funds_col_width,
										};
										fund_sheet.set_column(1, length as u16, funds_col_width, None).unwrap();
									}
									fund_sheet.write_string(0, col as WorksheetCol + 1, name, Some(&f_bold)).unwrap();
									for (row, rate) in rates.iter().enumerate() {
										let rate = rate.clone()*1_000_000.0;
										// if rate < 0.0 {
										// 	let rate = rate * -1.0;
										// 	let rate_str = format!("({rate})");
										// 	fund_sheet.write_string(row as WorksheetRow+1,  col as WorksheetCol + 1, rate_str.as_str(), None).unwrap();
										// } else {
											fund_sheet.write_number(row as WorksheetRow+1,  col as WorksheetCol + 1, rate, None).unwrap();
										// }
									}
									for (row, time) in times.iter().enumerate() {
										let time = time.replace("T", " ");
										let time = time.trim_end_matches(":00+00:00");
										fund_sheet.write_string(row as WorksheetRow+1,  0, time, None).unwrap();
									}
									col = col + 1;
								} else {
									continue;
								}		
							},
							None => {
								log.push((String::from("No data could be found"), (col+err_offset, err_row)));
								eprintln!("No data could be found for symbol \"{name}\"");
								err_offset = err_offset + 1;
								err_row = err_row + 1;
							},
						}
					},
					None => {
						eprintln!("No data with name {name} found in results");
					}
				}
			}
		},
		false => {},
	}
	// Log all errors
	if log.len() > 0 {
		let mut log_sheet = book.add_worksheet(Some("Log")).unwrap();
		if call_results.keys().len() > 0 {
			log_sheet.set_column(0, call_results.keys().len() as u16, 12.0, None).unwrap();
		}
		for (col, name) in names.iter().enumerate() {
			if call_results.contains_key(name) {
				log_sheet.write_string(0, col as WorksheetCol, name, Some(&f_bold)).unwrap();
				log_sheet.write_string(1, col as WorksheetCol, "No Errors", None).unwrap();
			}
		}
		for (logged, (col, row)) in log.iter() {
			log_sheet.write_string(*row as WorksheetRow+1, *col as WorksheetCol, logged, None).unwrap()
		}
	}
}
