// ~/api_calls.rs
//-----------------------------------------------

use std::collections::HashMap;
use crate::ftx::*;
use crate::spreadsheet_types::*;
use crate::util_yahoofinance::*;
use crate::yahoofinance::*;
use crate::coingecko::*;


//-----------------------------------------------
// Funcitions

// Actually call APIs

pub async fn parse_api_calls(mut input: HashMap<String, Option<ApiCall>>) -> HashMap<String, Option<ApiCall>> {
	for call in input.values_mut() {
		match call {
			Some(call) => {
				match call.api_source {
					ApiSource::YahooFinance => {
						let desires = &call.desired_data;
						// println!("Searching for ticker with input '{}'", call.id);
						let (ticker, valid) = check_valid_yahoo_ticker(&call.id).await;
						if valid {
							let quote = match yahoo_get_quotes(&ticker).await {
								Some(q) => q,
								None => continue,
							};
							let price: Option<Vec<f64>> =  match desires.contains(&DesiredData::Price) {
								true => Some(quote.close.clone().into_iter().map(|p| p).collect()),
								false => None,
							};
							let volume: Option<Vec<f64>> = match desires.contains(&DesiredData::Volume) {
								true => Some(quote.volume.into_iter().map(|v| v as f64).collect()),
								false => None,
							};
							let results = DataResults::from_values(price, volume, None, None);
							call.results = results;
						}
					},
					ApiSource::CoinGecko => {
						let id = match cg_search_asset_id(&call.id).await {
							Some(id) => id,
							None => continue,
						};
						let market_chart = match cg_get_market_chart(&id).await {
							Some(mc) => mc,
							None => continue,
						};
						let desires = &call.desired_data;
						let price: Option<Vec<f64>> = match desires.contains(&DesiredData::Price) {
							true => Some(market_chart.prices.into_iter().map(|p| p).collect()),
							false => None,
						};
						let volume: Option<Vec<f64>> = match desires.contains(&DesiredData::Volume) {
							true => Some(market_chart.total_volumes.into_iter().map(|v| v).collect()),
							false => None,
						};
						let results = DataResults::from_values(price, volume, None, None);
						call.results = results;
					}
					ApiSource::Ftx => {
						let id = match ftx_search_market_name(&call.id).await {
							Some(id) => id,
							None => continue,
						};
						let desires = &call.desired_data;
						let market = match ftx_get_market_history(&id).await {
							Some(m ) => m,
							None => continue,
						};
						let price = match desires.contains(&DesiredData::Price) {
							true => {
								let prices: Vec<f64> = market.result.clone().into_iter().map(|r| r.price).collect::<Vec<f64>>();
								Some(prices)
							},
							false => None,
						};
						let volume = match desires.contains(&DesiredData::Volume) {
							true => {
								let volume: Vec<f64> = market.result.into_iter().map(|r| r.volume).collect::<Vec<f64>>();
								Some(volume)
							},
							false => None,
						};

						let funding_rate: Option<(Vec<f64>, Vec<String>)> = match desires.contains(&DesiredData::FundingRate) {
							true => {
								match ftx_get_funding_rates(&id).await {
									Some(results) => {
										let rates = results.result.iter().map(|r| r.rate).collect::<Vec<f64>>();
										let times = results.result.iter().map(|r| r.to_owned().time).collect::<Vec<String>>();
										Some((rates, times))
									},
									None => None,
								}
							},
							false => None,
						};

						let mut open_interest: Option<(Vec<f64>, Vec<String>)> = None;
						if desires.contains(&DesiredData::OpenInterest) {
							open_interest = None;
						}
						let results = DataResults::from_values(price, volume, funding_rate, open_interest);
						call.results = results;
					},
					_ => {
						eprintln!("ERROR for Asset, '{}':\n API Source: {}\n Data To Pull: {:?}", call.id, call.api_source, call.desired_data);
					},
				}
			},
			None => {
			}
		}
	}
	input
}

// Parse all data from the spreadsheet and
// determine which APIs to use

#[allow(unused_assignments)]
pub fn parse_read_rows(read_data: Vec<ReadDataRow>) -> HashMap<String, Option<ApiCall>> {
	let mut calls: HashMap<String, Option<ApiCall>> = HashMap::new();
	for row in read_data.iter() {
		let mut is_error: bool;
		let desired_api = match row.asset_class {
			AssetClass::Commodity | AssetClass::Equity => {
				vec![ApiSource::YahooFinance]
			},
			AssetClass::Crypto | AssetClass::CryptoFuture | AssetClass::CryptoPerp => {
				vec![ApiSource::CoinGecko, ApiSource::Ftx]
			}
			AssetClass::Error => {
				is_error = true;
				vec![ApiSource::Error]
			}
		};
		match row.api_source {
			ApiSource::YahooFinance => {
				if desired_api.contains(&ApiSource::YahooFinance) == true && desired_api.contains(&ApiSource::Error) == false && (desired_api.contains(&ApiSource::CoinGecko) || desired_api.contains(&ApiSource::Ftx)) == false {
					// println!("No Errors for {:?}", row);
					let api_call = ApiCall::new(&row.symbol, &row.api_source, &row.desired_data);
					calls.insert(row.symbol.clone(), Some(api_call));
					continue;
				} else {
					is_error = true;
				}
			},
			ApiSource::CoinGecko | ApiSource::Ftx => {
				if (desired_api.contains(&ApiSource::CoinGecko) || desired_api.contains(&ApiSource::Ftx)) == true && desired_api.contains(&ApiSource::Error) == false && desired_api.contains(&ApiSource::YahooFinance) == false {
					// println!("No Errors for {:?}", row);
					let api_call = ApiCall::new(&row.symbol, &row.api_source, &row.desired_data);
					calls.insert(row.symbol.clone(), Some(api_call));
					continue;
				} else {
					is_error = true;
				}
			},
			ApiSource::Error => {
				is_error = true;
			},
		}

		if is_error {
			eprintln!("ERROR: Cannot make API calls for Asset Class '{}' with API Source '{}': '{}'", row.asset_class, row.api_source, row.symbol);
			calls.insert(row.symbol.clone(), None);
		}
	}
	calls
}
