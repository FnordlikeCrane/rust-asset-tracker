// ~/spreadsheet_types.rs
//-----------------------------------------------
// For Reading Spreadsheets


//-----------------------------------------------
// Structs

// API Call, the struct that is made from parsing
// data from the spreadsheet in order to send to 
// a website to get data
#[derive(Debug, Clone, PartialEq)]
pub struct DataResults {
	pub price: Option<Vec<f64>>,
	pub volume: Option<Vec<f64>>,
	pub funding_rate: Option<(Vec<f64>, Vec<String>)>,
	pub open_interest: Option<(Vec<f64>, Vec<String>)>,
	pub log: Vec<String>,
}
impl DataResults{
	pub fn from_values(price: Option<Vec<f64>>, volume: Option<Vec<f64>>, funding_rate: Option<(Vec<f64>, Vec<String>)>, open_interest: Option<(Vec<f64>, Vec<String>)>) -> DataResults {
		DataResults{price, volume, funding_rate, open_interest, log: Vec::new()}
	}
}
impl Default for DataResults {
	fn default() -> Self {
		DataResults{price: None, volume: None, funding_rate: None, open_interest: None, log: Vec::new()}
	}
}

#[derive(Debug, Clone, PartialEq)]
pub struct ApiCall {
	pub id: String,
	pub api_source: ApiSource,
	pub desired_data: Vec<DesiredData>,
	pub results: DataResults,
}
impl ApiCall {
	pub fn new(id: &String, api_source: &ApiSource,	desired_data: &Vec<DesiredData>) -> ApiCall {
		ApiCall{id: id.clone(), api_source: api_source.clone(), desired_data: desired_data.clone(), results: DataResults::default()}
	}
}
impl Default for ApiCall {
	fn default() -> Self {
		ApiCall{id: String::new(), api_source: ApiSource::default(), desired_data: Vec::new(), results: DataResults::default()}
	}
}

// Read Data from Row
// struct that contains data read from the spreadsheet

#[derive(Debug, Clone)]
pub struct ReadDataRow {
	pub symbol: String,
	pub asset: String,
	pub asset_class: AssetClass,
	pub api_source: ApiSource,
	pub desired_data: Vec<DesiredData>,
}
impl Default for ReadDataRow {
	fn default() -> Self {
		ReadDataRow{symbol: String::new(), asset: String::from("NONE"), asset_class: AssetClass::Error, api_source: ApiSource::Error, desired_data: Vec::new()}
	}
}
impl ReadDataRow {
	pub fn from_raw_data(symbol: &String, asset: &String, asset_class: &String, api_source: &String, desired_data: &String) -> ReadDataRow {
		let asset_class = eval_asset_class(&asset_class);
		let api_source = eval_api_source(&api_source);
		let all_desired_data = desired_data.split(",");
		let mut desired_data = Vec::new();
		for parsed_data in all_desired_data {
			let parsed_data = eval_desired_data(&String::from(parsed_data));
			desired_data.push(parsed_data);
		}
		ReadDataRow { symbol: symbol.clone(), asset: asset.clone(), asset_class, api_source, desired_data }
	}
}


//-----------------------------------------------
// Enums

// API Source

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ApiSource {
	YahooFinance,
	CoinGecko,
	Ftx,
	Error,
}

impl Default for ApiSource {
	fn default() -> Self {
		ApiSource::Error
	}
}

impl std::fmt::Display for ApiSource {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            ApiSource::YahooFinance => "Yahoo Finance".fmt(f),
            ApiSource::CoinGecko => "CoinGecko".fmt(f),
			ApiSource::Ftx => "FTX".fmt(f),
			ApiSource::Error => "Error, Invalid API Source".fmt(f),
        }
    }
}

pub fn eval_api_source(input: &String) -> ApiSource {
	let input = input.replace(" ", "");
	match input.to_lowercase().as_str() {
		"yahoo" | "yahoofinance" => ApiSource::YahooFinance,
		"coingecko" | "gecko" => ApiSource::CoinGecko,
		"ftx" => ApiSource::Ftx,
		_ => ApiSource::Error,
	}
}


// Asset Classes

#[derive(Debug, Clone, Copy)]
pub enum AssetClass {
	Commodity,
	Crypto,
	Equity,
	CryptoPerp,
	CryptoFuture,
	Error,
}

impl Default for AssetClass {
	fn default() -> Self {
		AssetClass::Error
	}
}

pub fn eval_asset_class(input: &String) -> AssetClass {
	let input = input.replace(" ", "");
	match input.to_lowercase().as_str() {
		"commodity" => AssetClass::Commodity,
		"crypto" | "cryptocurrency" => AssetClass::Crypto,
		"equity" => AssetClass::Equity,
		"cryptoperp" => AssetClass::CryptoPerp,
		"cryptofuture" => AssetClass::CryptoFuture,
		_ => AssetClass::Error,
	}
}

impl std::fmt::Display for AssetClass {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            AssetClass::Commodity => "Commodity".fmt(f),
			AssetClass::Crypto => "Cryptocurrency".fmt(f),
			AssetClass::Equity => "Equity".fmt(f),
			AssetClass::CryptoPerp => "Crypto Perp".fmt(f),
			AssetClass::CryptoFuture => "Crypto Future".fmt(f),
			AssetClass::Error => "Error, Invalid Asset Class".fmt(f),
        }
    }
}


// Desired Data Types to Scrape

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum DesiredData {
	Price,
	Volume,
	FundingRate,
	OpenInterest,
	Error
}

impl Default for DesiredData {
	fn default() -> Self {
		DesiredData::Error
	}
}

pub fn eval_desired_data(input: &String) -> DesiredData {
	let input = input.replace(" ", "");
	match input.to_lowercase().as_str() {
		"price" => DesiredData::Price,
		"volume" => DesiredData::Volume,
		"fundingrate" => DesiredData::FundingRate,
		"openinterest" => DesiredData::OpenInterest,
		_ => DesiredData::Error,
	}
}

impl std::fmt::Display for DesiredData {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            DesiredData::Price => "Price".fmt(f),
			DesiredData::Volume => "Volume".fmt(f),
			DesiredData::FundingRate => "Funding Rate".fmt(f),
			DesiredData::OpenInterest => "Open Interest".fmt(f),
			DesiredData::Error => "Error, Invalid Desired Data".fmt(f),
        }
    }
}
