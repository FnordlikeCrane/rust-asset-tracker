// ~/ftx.rs
//-----------------------------------------------

use serde::{Deserialize, Serialize};
use rustc_serialize::hex::ToHex;
use std::path::Path;
use std::time::SystemTime;
use reqwest::StatusCode;
use reqwest;
use sha2::{Sha256, Digest};
use rust_fuzzy_search::fuzzy_compare;

use crate::util::CONFIG_PATH;
use crate::config::*;


//-----------------------------------------------
// Functions for Key Signing

fn compute_block_sized_key(mut key: Vec<u8>, block_size: u32) -> Vec<u8> {
	if key.len() > block_size as usize {
		let mut sha256 = Sha256::new();
		sha256.update(key.as_slice());
		key = sha256.finalize().to_vec();
	}
	if key.len() < block_size as usize{
		key.resize_with(block_size as usize, Default::default);
	}

	let key = key.clone();
	key
}

fn bitwise_vec(a: Vec<u8>, b: Vec<u8>) -> Vec<u8> {
	assert_eq!(a.len(), b.len());
	a.iter()
		.zip(b.iter())
		.map(|(x, y)| *x ^ *y)
		.collect()
}

fn hmac_sha256(key: Vec<u8>, message: Vec<u8>) -> Vec<u8> {
	let block_size: u32 = 64;

	let block_sized_key = compute_block_sized_key(key, block_size);
	
	let o_pad: Vec<u8> = (0..block_sized_key.len()).map(|_| 0x5c as u8).collect();
	let i_pad: Vec<u8> = (0..block_sized_key.len()).map(|_| 0x36 as u8).collect();
	
	let mut o_key_pad = bitwise_vec(block_sized_key.clone(), o_pad);
	let mut i_key_pad = bitwise_vec(block_sized_key, i_pad);
	
	i_key_pad.extend_from_slice(&message);
	let mut sha256 = Sha256::new();
	sha256.update(i_key_pad.as_slice());
	let concat1 = sha256.finalize().to_vec();

	o_key_pad.extend_from_slice(&concat1);
	let mut sha256 = Sha256::new();
	sha256.update(o_key_pad.as_slice());
	let concat2 = sha256.finalize().to_vec();
	concat2
}

//-----------------------------------------------
// REST Header Strcuts

#[derive(Deserialize, Serialize, Debug)]
struct FtxHeader {
	#[serde(rename = "FTX-KEY")]
	pub ftx_key: String,
	#[serde(rename = "FTX-SIGN")]
	pub ftx_sign: String, //[u8; 32],
	#[serde(rename = "FTX-TS")]
	pub ftx_ts: u64,
}

impl FtxHeader {
	pub fn new(input: &String, api_key: &String, secret_key: &String) -> FtxHeader {
		let secret = secret_key.as_bytes();
	
		let timestamp = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH);
		let timestamp = match timestamp {
			Ok(ts) => ts.as_secs(),
			Err(_) => 0,
		};
		
		let sign = format!("{{{}}}{{GET}}{{{}}}", timestamp.clone(), input);
		let sign = Vec::from(sign.as_bytes().to_hex().as_bytes());
		let encoded = hmac_sha256(Vec::from(secret), sign);
		let ftx_sign = String::from_utf8_lossy(&encoded);
		let ftx_sign = String::from(ftx_sign);
		let ftx_sign = ftx_sign.as_bytes().to_hex();

		FtxHeader{ftx_key: api_key.to_string(), ftx_sign, ftx_ts: timestamp}
	}
}


//-----------------------------------------------
// Structs for pulling data

// Market Histroy Structs

// For a single day of history
#[derive(Deserialize, Debug, Clone)]
pub struct FtxMarketHistoryDay {
	#[serde(rename="close")]
	pub price: f64,
	pub volume: f64,
	#[serde(rename="startTime")]
	pub date: String,
}

// For the restults of GET /markets/{market}/candle
#[derive(Deserialize, Debug, Clone)]
pub struct FtxMarketHistoryResult {
	pub result: Vec<FtxMarketHistoryDay>,
	pub success: bool,
}

// For a single market to determine name input
#[derive(Deserialize, Debug, Clone)]
pub struct FtxSingleMarketResult {
	pub name: String,
}

// For the results of GET /markets
#[derive(Deserialize, Debug, Clone)]
pub struct FtxAllMarketsResult {
	pub result: Vec<FtxSingleMarketResult>,
	pub success: bool,
}


// For a single funding rate result
#[derive(Deserialize, Debug, Clone)]
pub struct FtxSingleFundingRate {
	pub future: String,
	pub rate: f64,
	pub time:  String,
}

// For the results of GET /funding_rates?future={name}
#[derive(Deserialize, Debug, Clone)]
pub struct FtxFundingRatesResult {
	pub result: Vec<FtxSingleFundingRate>,
	pub success: bool,
}


//-----------------------------------------------
// Functions for pulling data

// Get funding rates
pub async fn ftx_get_funding_rates(name: &String) -> Option<FtxFundingRatesResult> {
	let path = Path::new(CONFIG_PATH);
	if path.is_file() == false {
		println!("No Config file detected. Creating the default one.");
		make_default_conf_file();
	}
	let config = Config::from_file(CONFIG_PATH.to_string());
	let api_key = config.ftx_config.api_key.clone();
	let secret_key = String::from(config.ftx_config.get_secret());
	if api_key == "" {
		return None;
	}
	if secret_key == "" {
		return None;
	}
	// Build a REST payload
	let uri_path = format!("/api/funding_rates?future={name}");
	let header = FtxHeader::new(&uri_path, &api_key, &secret_key);

	let url = format!("https://ftx.com{}", uri_path);
	let payload = serde_json::to_string(&header).unwrap();

	let client = reqwest::Client::new();
	let response = match  client.get(url.as_str()).body(payload.clone()).send().await {
		Ok(r) => {
			r
		},
		Err(e) => {
			eprintln!("ERROR: URL Connection, \"{url}\" resulted in error: {e}");
			return None
		},
	};
	// let https = HttpsConnector::new();
	// let client = Client::builder().build::<_, hyper::Body>(https);

	// let req = Request::builder().method(Method::GET).uri(url).body(Body::from(payload)).unwrap();
	// let resp = client.request(req).await.unwrap();
	// let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
	let result: FtxFundingRatesResult = match response.status() {
		StatusCode::OK => {
			let json = response.json().await;
			// println!("{:?}", json);
			match json {
				Ok(r) => r,
				Err(e) => {
					eprintln!("ERROR: {e}, for symbol \"{name}\"");
					return None
				},
			}
		},
		status => {
			eprintln!("Bad Response Code: {:?}", status);
			return None
		},
	};
	Some(result)
}

// Search for an Asset 
pub async fn ftx_search_market_name(name: &String) -> Option<String> {
	// Ensure that there are valid keys to make any calls
	let path = Path::new(CONFIG_PATH);
	if path.is_file() == false {
		println!("No Config file detected. Creating the default one.");
		make_default_conf_file();
	}
	let config = Config::from_file(CONFIG_PATH.to_string());
	let api_key = config.ftx_config.api_key.clone();
	let secret_key = String::from(config.ftx_config.get_secret());
	if api_key == "" {
		return None;
	}
	if secret_key == "" {
		return None;
	}

	// Build a REST call to see list of all markets
	let uri_path = format!("/api/markets");
	let header = FtxHeader::new(&uri_path, &api_key, &secret_key);

	let url = format!("https://ftx.com{}", uri_path);
	let payload = serde_json::to_string(&header).unwrap();
	
	let client = reqwest::Client::new();
	let response = match  client.get(url.as_str()).body(payload.clone()).send().await {
		Ok(r) => {
			r
		},
		Err(e) => {
			eprintln!("ERROR: URL Connection, \"{url}\" resulted in error: {e}");
			return None
		},
	};

	// List of all results:
	let results: FtxAllMarketsResult = match response.status() {
		StatusCode::OK => {
			let json = response.json().await;
			match json {
				Ok(r) => r,
				Err(e) => {
					eprintln!("ERROR: {e}, for symbol \"{name}\"");
					return None
				},
			}
		},
		status => {
			eprintln!("Bad Response Code: {:?}", status);
			return None
		},
	};
	
	let mut high_score: f32 = 0.0;
	let mut high_string = String::new();
	for result in results.result.iter() {
		let score = fuzzy_compare(name.to_lowercase().as_str(), result.name.to_lowercase().as_str());
		if score == 1.0 {
			return Some(name.clone());
		} else if score > high_score {
			high_score = score;
			high_string = result.name.clone();
		}
	}

	if high_score <= 0.5 {
		return None;
	} else {
		println!("WARN: Partial match for \"{name}\": \"{high_string}\"");
		return Some(high_string);
	}
}

pub async fn ftx_get_market_history(name: &String) -> Option<FtxMarketHistoryResult> {
	// Ensure that there are valid keys to make any calls
	let path = Path::new(CONFIG_PATH);
	if path.is_file() == false {
		println!("No Config file detected. Creating the default one.");
		make_default_conf_file();
	}
	let config = Config::from_file(CONFIG_PATH.to_string());
	let api_key = config.ftx_config.api_key.clone();
	let secret_key = String::from(config.ftx_config.get_secret());
	if api_key == "" {
		return None;
	}
	if secret_key == "" {
		return None;
	}

	let uri_path = format!("/api/markets/{}/candles?resolution=86400", name);
	let header = FtxHeader::new(&uri_path, &api_key, &secret_key);
	let url = format!("https://ftx.com{}", uri_path);
	let payload = serde_json::to_string(&header).unwrap();
	let client = reqwest::Client::new();
	let response = match  client.get(url.as_str()).body(payload.clone()).send().await {
		Ok(r) => {
			r
		},
		Err(e) => {
			eprintln!("ERROR: URL Connection, \"{url}\" resulted in error: {e}");
			return None
		},
	};
	
	let result: FtxMarketHistoryResult = match response.status() {
		StatusCode::OK => {
			let json = response.json().await;
			match json {
				Ok(r) => r,
				Err(e) => {
					eprintln!("ERROR: {e}, for symbol \"{name}\"");
					return None
				},
			}
		},
		status => {
			eprintln!("Bad Response Code: {:?}", status);
			return None
		},
	};
	Some(result)
}