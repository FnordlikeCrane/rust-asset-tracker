// ~/util_yahoofinance.rs
//-----------------------------------------------

use crate::yahoofinance::*;


//-----------------------------------------------
// Util Functions for Yahoo Finance

pub async fn check_valid_yahoo_ticker(searching: &String) -> (String, bool) {
	let find = yahoo_search_ticker(&searching).await;
	match find {
		Some(t) => (t, true),
		None => (format!("Ticker not found for {searching}"), false),
	}
}
