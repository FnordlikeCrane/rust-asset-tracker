// ~/main.rs
//-----------------------------------------------

mod read_spreadsheet;
mod write_spreadsheet;
mod config;
mod coingecko;
mod yahoofinance;
mod util;
mod util_yahoofinance;
mod spreadsheet_types;
mod api_calls;
mod ftx;

use config::*;
use write_spreadsheet::*;
use read_spreadsheet::*;
use api_calls::*;
use util::*;

use std::path::Path;
use xlsxwriter::*;
use chrono::prelude::*;

//-----------------------------------------------
// Main Function

#[tokio::main]
async fn main() {
	let path = Path::new(CONFIG_PATH);
	if path.is_file() == false {
		println!("No Config file detected. Creating the default one.");
		make_default_conf_file();
	}

	let config = Config::from_file(CONFIG_PATH.to_string());

	if config.debug_config.print_runtimes == true {
		let time = Local::now();
		println!("LOG: Time after reading config: \"{}\"", time.format("%H:%M:%S-%f"));
	}

	let path = Path::new(config.input_sheet.file_name.as_str());
	let path_string = match path.as_os_str().to_str() {
		Some(p) => String::from(p),
		None => {
			eprintln!("ERROR: Could not translate path to spreadsheet to a valid OS String");
			return
		}
	};
	if path.is_file() == false {
		println!("Could not find input spreadsheet...");
		match make_default_sheet(&path_string, &config.input_sheet.page_name) {
			Ok(()) => {	println!("Created a new spreadsheet to track assets from:\nNew file: \"{}\"", path_string); },
			Err(e) => { eprint!("ERROR: Could not create default input spreadsheet (\"{path_string}\"):\n{e}"); },
		};
	}

	let (input_names, data) = get_data_from_rows(&path_string, &config.input_sheet.page_name);
	let calls = parse_read_rows(data);
	
	if config.debug_config.print_runtimes == true {
		let time = Local::now();
		println!("LOG: Time reading Input Spreadsheet: \"{}\"", time.format("%H:%M:%S-%f"));
	}
	
	let results = parse_api_calls(calls).await;
	
	if config.debug_config.print_runtimes == true {
		let time = Local::now();
		println!("LOG: Time making all API calls: \"{}\"", time.format("%H:%M:%S-%f"));
	}

	let book = Workbook::new(config.output_sheet.file_name.as_str());
	write_market_history(&book, &input_names, &results, &config);
	
	if config.debug_config.print_runtimes == true {
		let time = Local::now();
		println!("LOG: Time writing Output Spreadsheet: \"{}\"", time.format("%H:%M:%S-%f"));
	}
}
