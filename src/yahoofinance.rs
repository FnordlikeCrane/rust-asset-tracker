// ~/yahoofinance.rs
//-----------------------------------------------

use chrono::{Utc,TimeZone};
use rust_fuzzy_search::fuzzy_compare;
use serde::Deserialize;
use serde_json::{Value, Map};
use reqwest::StatusCode;
use reqwest;
use async_compat::CompatExt;


//-----------------------------------------------
// Yahoo Finance Structs

//  For getting prices and volumes
#[derive(Debug, Deserialize, Clone, Default)]
pub struct Quote {
	pub volume: Vec<u64>,
    pub close: Vec<f64>,
}

/* // No longer necessary because of errors trying to use this method
#[derive(Deserialize, Debug, Clone, Default)]
struct YFResultIndicators {
	pub quote: Option<Vec<Quote>>,
}

#[derive(Deserialize, Debug, Default)]
struct YFChartResult {
	pub indicators: YFResultIndicators,
}

#[derive(Deserialize, Debug)]
struct YFChart {
	pub result: Vec<YFResultIndicators>,
	// pub result: Map<String, Value>,
}

#[derive(Deserialize, Debug)]
struct YFQuotesResults {
    pub chart: YFChart,
}
*/
// For a symbol query  to find the correct name of the asset
#[derive(Deserialize, Debug)]
struct YFSingleQuery {
	pub symbol: String,
}

#[derive(Deserialize, Debug)]
struct YFQueryResults {
    pub quotes: Vec<YFSingleQuery>,
}


//-----------------------------------------------
// Functions

pub async fn yahoo_get_quotes(symbol: &String) -> Option<Quote> {
	let start = Utc.ymd(2013, 1, 1).and_hms_milli(0, 0, 0, 0).timestamp();
    let end = Utc::now().timestamp();
	let query_url = format!("https://query1.finance.yahoo.com/v8/finance/chart/{symbol}?symbol={symbol}&period1={start}&period2={end}&interval=1d");
	// println!("{query_url}");
	let resp = reqwest::get(query_url.as_str()).compat().await;
    if resp.is_err() {
		eprintln!("Could not connnect to {query_url}");
        return None;
    }
	let resp = resp.unwrap();
	// let results: YFQuotesResults = match resp.status() {
	let results: Value = match resp.status() {
		StatusCode::OK => {
			let json = resp.json().await;
			// println!("{:?}", json);
			match json {
				Ok(r) => r,
				Err(e) => {
					eprintln!("ERROR: {e}, for symbol \"{symbol}\"");
					return None
				},
			}
		},
		status => {
			eprintln!("Bad Response Code: {:?}", status);
			return None
		}
	};
	
	// println!("{:?}", results);
	let query = match results.as_object() {
		Some(q) => {
			q.to_owned()
		},
		None => return None,
	};
	let mut chart: Map<String, Value> = Map::new();
	for (_k, v) in query.iter() {
		// println!("{_k}");
		chart = match v.as_object() {
			Some(c) => c.to_owned(),
			None => return None,
		};
	}
	let results: Vec<Map<String, Value>> = match chart.get(&String::from("result")) {
		Some(r) => {
			match r.as_array() {
				Some(r) => {
					let array = r.to_owned();
					let array = array.into_iter().map(|v| { let o = match v.as_object() {
							Some(o) => o.to_owned(),
							None => Map::new(),
						};
						o
					}).collect();
					array
				},
				None => return None,
			}
		},
		None => return None,
	};
	let indicators: Map<String, Value>;
	match results.iter().next()  {
		Some(m) => {
			indicators = match m.get(&String::from("indicators")) {
				Some(i) => {
					let m = i.as_object();
					match m {
						Some(m) => m.to_owned(),
						None => return None,
					}
				},
				None => {
					return None
				},
			};
		},
		None => return None,
	}
	
	match indicators.get(&String::from("quote")) {
		Some(v) => {
			let quote: Vec<Quote> = match serde_json::from_value(v.to_owned()) {
				Ok(q) => q,
				Err(e) => {
					eprintln!("ERROR: {e}");
					return None;
				},
			};
			match quote.get(0) {
				Some(q) => return Some(q.to_owned()),
				None => return None,
			}
		},
		None => return None,
	}
}

pub async fn yahoo_search_ticker(search: &String) -> Option<String> {
	let lower_search = search.to_lowercase();
	// let input = lower_search.replace("^", "%5e");
	let search_url = format!("https://query2.finance.yahoo.com/v1/finance/search?q={search}");
	let resp = reqwest::get(search_url.as_str()).compat().await;
    if resp.is_err() {
        return None;
    }
	let resp = resp.unwrap();
	let results: YFQueryResults = match resp.status() {
		StatusCode::OK => {
			resp.json().await.unwrap()
		},
		_ => return None
	};

	let compare_vec: Vec<String> = results.quotes.iter().map(|x| x.symbol.clone()).collect();

	let mut high_score: f32 = 0.0;
	let mut high_string = String::new();
	
	for symbol in compare_vec.iter() {
		let score = fuzzy_compare(lower_search.as_str(), symbol.to_lowercase().as_str());
		if score == 1.0 {
			return Some(search.clone());
		} else if score > high_score {
			high_score = score;
			high_string = symbol.clone();
		}
	}

	if high_score <= 0.5 {
		return None;
	} else {
		if high_score != 1.0 {
			println!("WARN: Partial match for {search}: {high_string} = ({high_score})");
		}
		return Some(high_string);
	}
}
