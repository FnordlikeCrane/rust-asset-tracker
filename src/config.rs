// ~/config.rs
//----------------------------------------------------
// Config Structs for serialzing and desializing data

use std::fmt::Debug;

use serde::{Serialize, Deserialize};
use crate::util::CONFIG_PATH;

#[derive(Deserialize, Serialize, Debug)]
pub struct FtxConfig {
	#[serde(rename = "api-key")]
	pub api_key: String,
	#[serde(rename = "secret-key")]
	secret_key: String,
}

impl FtxConfig {
	pub fn get_secret(&self) -> &str {
		self.secret_key.as_str()
	}
}

impl Default for FtxConfig {
	fn default() -> Self {
		FtxConfig{api_key: String::new(), secret_key: String::new()}
	}
}

#[derive(Deserialize, Serialize, Debug)]
pub struct OutputSheetConfig {
	#[serde(rename = "file-name")]
	pub file_name: String
}

impl Default for OutputSheetConfig {
	fn default() -> Self {
		OutputSheetConfig{file_name: "Output.xlsx".to_string()}
	}
}

#[derive(Deserialize, Serialize, Debug)]
pub struct InputSheetConfig {
	#[serde(rename = "file-name")]
	pub file_name: String,
	#[serde(rename = "page-name")]
	pub page_name: String,
}

impl Default for InputSheetConfig {
	fn default() -> Self {
		InputSheetConfig{file_name: "Input.xlsx".to_string(), page_name: "Assets".to_string()}
	}
}

#[derive(Deserialize, Serialize, Debug)]
pub struct FormattingConfig {
	#[serde(rename = "volume-column-width")]
	pub vol_col_width: f64,
	#[serde(rename = "price-column-width")]
	pub price_col_width: f64,
	#[serde(rename = "funding-rate-column-width")]
	pub funds_col_width: f64,
	#[serde(rename = "date-column-width")]
	pub date_col_width: f64,
	#[serde(rename = "long-date-column-width")]
	pub long_date_col_width: f64,
}

impl Default for FormattingConfig {
	fn default() -> Self {
		FormattingConfig{
			vol_col_width: 8.0,
			price_col_width: 10.0,
			funds_col_width: 6.4,
			date_col_width: 10.9,
			long_date_col_width: 15.8,
		}
	}
}

#[derive(Deserialize, Serialize, Debug)]
pub struct DebugConfig {
	#[serde(rename = "print-runtimes")]
	pub print_runtimes: bool,
	#[serde(rename = "log-runtimes")]
	pub log_runtimes: bool,
}

impl Default for DebugConfig {
	fn default() -> Self {
		DebugConfig {
			print_runtimes: false,
			log_runtimes: false,
		}
	}
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Config {
	#[serde(rename = "InputSheet")]
	pub input_sheet: InputSheetConfig,
	#[serde(rename = "OutputSheet")]
	pub output_sheet: OutputSheetConfig,
	#[serde(rename = "FtxConfig")]
	pub ftx_config: FtxConfig,
	#[serde(rename = "Formatting")]
	pub formatting_config: FormattingConfig,
	#[serde(rename = "Debugging")]
	pub debug_config: DebugConfig,
}

impl Default for Config {
	fn default() -> Self {
		Config{
			input_sheet: InputSheetConfig::default(),
			output_sheet: OutputSheetConfig::default(),
			ftx_config: FtxConfig::default(),
			formatting_config: FormattingConfig::default(),
			debug_config: DebugConfig::default(),
		}
	}
}

impl Config {
	pub fn from_file(path: String) -> Config {
		let contents = std::fs::read_to_string(path).expect("Could not open Config file");
		let config: Config = toml::from_str(contents.as_str()).expect("Config file contains errors");
		config
	}
}


pub fn make_default_conf_file() {
	let file = Config::default();
	let toml_string = toml::to_string(&file).expect("Could not serialize config");
	let toml_string = format!("# Config File: Intended to be human readable, using TOML formatting\n#--------------------------------------------------------------------\n\n{}", toml_string);

	use std::fs::File;
	use std::io::prelude::Write;
	let mut file = File::create(CONFIG_PATH).expect("Could not create config file");
	file.write_all(toml_string.as_bytes()).expect("Could not write config to file");
}