// ~/util_google_sheets.rs
//-----------------------------------------------

use hyper::Client;
use hyper_tls::HttpsConnector;
use crate::config::*;
use crate::read_google_sheets::*;
use crate::util::debug_on;

/*
#[derive(Debug)]
pub enum GSheetColumnVals {
	A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, ERROR
}
*/

//-----------------------------------------------
// Consts and Enums

pub const GSHEET_COLUMN: &'static str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";


//-----------------------------------------------
// Structs

#[derive(Clone, Copy)]
pub struct Range {
	pub start_col: char,
	pub start_row: u16,
	pub end_col: char,
	pub end_row: u16,
}

impl Range {
	pub fn as_string(&self) -> String {
		format!("{}{}:{}{}", self.start_col, self.start_row, self.end_col, self.end_row)
	}

	pub fn new() -> Range {
		// First Cell by default
		Range{start_col: 'A', start_row: 1, end_col: 'A', end_row: 1}
	}

	pub fn from_values(start_col: char, start_row: u16, end_col: char, end_row: u16) -> Range {
		Range{start_col, start_row, end_col, end_row}
	}
}

//-----------------------------------------------
// Util Functions

/*
fn get_columnchar_from_val(col: GSheetColumnVals) -> char {
	let search = GSHEET_COLUMN.as_bytes();
	search[col as usize] as char
}
*/

pub fn get_num_from_columnchar(searching: char) -> usize {
	for (index, val) in GSHEET_COLUMN.as_bytes().iter().enumerate() {
		let compare = val.clone() as char;
		if compare == searching {
			return index;
		}
	}
	GSHEET_COLUMN.len()
}

pub fn get_columnchar_from_num(num: usize) -> char {
	let search = GSHEET_COLUMN.as_bytes();
	search[num] as char
}

/*
pub fn get_columnval_from_num(num: usize) -> GSheetColumnVals {
	let val = (GSheetColumn.len() % num ) as u8;
	match val {
		0 => GSheetColumnVals::A,
		1 => GSheetColumnVals::B,
		2 => GSheetColumnVals::C,
		3 => GSheetColumnVals::D,
		4 => GSheetColumnVals::E,
		5 => GSheetColumnVals::F,
		6 => GSheetColumnVals::G,
		7 => GSheetColumnVals::H,
		8 => GSheetColumnVals::I,
		9 => GSheetColumnVals::J,
		10 => GSheetColumnVals::K,
		11 => GSheetColumnVals::L,
		12 => GSheetColumnVals::M,
		13 => GSheetColumnVals::N,
		14 => GSheetColumnVals::O,
		15 => GSheetColumnVals::P,
		16 => GSheetColumnVals::Q,
		17 => GSheetColumnVals::R,
		18 => GSheetColumnVals::S,
		19 => GSheetColumnVals::T,
		20 => GSheetColumnVals::U,
		21 => GSheetColumnVals::V,
		22 => GSheetColumnVals::W,
		23 => GSheetColumnVals::X,
		24 => GSheetColumnVals::Y,
		25 => GSheetColumnVals::Z,
		_ => GSheetColumnVals::ERROR
	}
}
*/

pub async fn get_gsheet_data_range(sheet_name: &String) -> Range {
	let config_path = format!("{}/config.conf", env!("CARGO_MANIFEST_DIR"));	
	let contents = std::fs::read_to_string(config_path).expect("Could not open Config file");

	let config: Config = toml::from_str(contents.as_str()).unwrap();
	let credentials: CredentialsConf = config.credentials;
	let api_key = credentials.api_key;

	let sheets_conf: SheetsConf = config.sheets;
	let spreadsheet_id = sheets_conf.spreadsheet_id;

	// Create the range string (the entire spreadsheet)
	let range_str = "A1:Z1000";

	// Get the URL for the sheet we want to poll
	let url = format!("https://sheets.googleapis.com/v4/spreadsheets/{}/values/{}!{}?key={}",
		spreadsheet_id,
		sheet_name,
		range_str,
		api_key
	);

	let uri = url.as_str().parse().unwrap();

	let https = HttpsConnector::new();
	let client = Client::builder().build::<_, hyper::Body>(https);	
	let resp = client.get(uri).await.unwrap();
	let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
	
	let payload: SheetReadPayload = serde_json::from_slice(&body).unwrap();

	let rows = payload.values;
	
	let num_rows = rows.len();
	let mut last_width: usize = 0;

	let mut start_depth: usize = 1;
	let mut last_start_width: usize = GSHEET_COLUMN.len();

	for row in rows.clone().iter() {
		let curr_width = row.len();
		if curr_width == 0 {
			if last_width == 0 {
				start_depth += 1;
			}
		} else {
			let mut curr_start_width: usize = GSHEET_COLUMN.len();
			for (i, cell) in row.iter().enumerate() {
				match cell {
					Some(val) => {
						if val.len() > 0 {
							curr_start_width = i.clone();
							break;
						} else {
							continue;							
						}
					},
					None => {
						continue;
					}
				}
			}
			if curr_start_width < last_start_width {
				last_start_width = curr_start_width.clone();
			}
		}
		if curr_width > last_width {
			last_width = curr_width;
		}
	}
	let start_width = get_columnchar_from_num(last_start_width);
	let width = get_columnchar_from_num(last_width-1);

	Range::from_values(start_width, start_depth as u16,  width, num_rows as u16)
}


//-----------------------------------------------
// Parse range functions

pub fn parse_range_for_assets(original: &Range, payload: &SheetReadPayload) -> Option<Range> {
	let values = payload.values.clone();
	if values.len() == 0 {
		return None;
	}
	
	let mut col_offset: Option<u16> = None;
	let mut row_offset: Option<u16> = None;
	for (row_num, row) in values.iter().enumerate() {
		// if debug_on() >= 4 { print!("row{}: ", row_num); }
		if row.len() == 0 {
			// if debug_on() >= 4 { print!("NONE "); }
			continue;
		}
		let mut last_cell = String::new();
		for (cell_num, cell) in row.iter().enumerate() {
			// if debug_on() >= 4 { print!("{:?} ", cell); }
			match cell {
				Some(cell) => {
					if last_cell.to_lowercase() == "asset" && cell.to_lowercase() == "value" {
						col_offset = Some(cell_num.clone() as u16);
						row_offset = Some(row_num.clone() as u16);
						break;
					} else {
						last_cell = cell.clone();					}
				},
				None => {
					last_cell = String::new();
				}
			}
		}
		// if debug_on() >= 4 { print!("\n"); }
	}

	let row_offset = match row_offset {
		Some(row_offset) => {
			row_offset
		},
		None => {
			return None;
		}
	};

	let col_offset = match col_offset {
		Some(col_offset) => {
			col_offset-1
		},
		None => {
			return None;
		}
	};

	let original_start_col_as_num: u16 = get_num_from_columnchar(original.start_col) as u16;
	let new_start_col = original_start_col_as_num+col_offset;
	let new_start_col = get_columnchar_from_num(new_start_col as usize);
	
	// let original_end_col_as_num: u16 = get_num_from_columnchar(original.end_col) as u16;
	let new_end_col = original_start_col_as_num+col_offset+1;
	let new_end_col = get_columnchar_from_num(new_end_col as usize);

	let new_start_row = original.start_row+row_offset;

	let new_range = Range::from_values(new_start_col, new_start_row, new_end_col, original.end_row);

	Some(new_range)
}
