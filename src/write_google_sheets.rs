// ~/write_google_sheets.rs
//-----------------------------------------------
#![allow(non_snake_case, dead_code)]

use openssl::sign::Signer;
use openssl::pkey::PKey;
use openssl::hash::MessageDigest;
use serde::{Serialize, Deserialize};
use std::time::SystemTime;
use data_encoding::BASE64URL;
use hyper::{Body, Method, Request, Client};
use hyper_tls::HttpsConnector;
use serde_json::Value;

use crate::config::*;
use crate::util_google_sheets::*;


//-----------------------------------------------
// Google Sheets Write Structs

#[derive(Deserialize, Serialize, Debug)]
struct JwtHeader {
	alg: String,
	typ: String,
}

impl JwtHeader {
	fn new() -> JwtHeader {
		JwtHeader{alg: String::from("RS256"), typ: String::from("JWT")}
	}

	fn as_json(&self) -> String {
		let header_json = serde_json::to_string(self).unwrap();
		header_json
	}

	fn as_b64url(&self) -> String {
		let header_json = self.as_json();
		let header_b = header_json.as_str().as_bytes();
		let header_base64 = BASE64URL.encode(header_b);
		header_base64
	}
}

#[derive(Deserialize, Serialize, Debug)]
struct JwtClaims {
	iss: String , // email address
	scope: String, // space delimited permission list
	aud: String, // always: "https://oauth2.googleapis.com/token"
	exp: u64, // expiration time as 1 hour + time issued in UTC Seconds
	iat: u64, // time issued in seconds
}

impl JwtClaims {
	fn new() -> JwtClaims {
		let now = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap();
		let expire = now.as_secs()+3600;

		// Get the email associated with the JWT from the config file
		let path = format!("{}/config.conf", env!("CARGO_MANIFEST_DIR"));
		// Read it
		let contents = std::fs::read_to_string(path).expect("Could not open Config file");
		// Gather the proper structs from the string
		let config: Config = toml::from_str(contents.as_str()).unwrap();
		let credentials: CredentialsConf = config.credentials;
		let email = credentials.client_email;	

		JwtClaims{
			iss: email,
			scope: "https://www.googleapis.com/auth/spreadsheets https://www.googleapis.com/auth/drive".to_string(),
			aud: "https://oauth2.googleapis.com/token".to_string(),
			exp: expire,
			iat: now.as_secs()
		}
	}

	fn as_json(&self) -> String {
		let header_json = serde_json::to_string(self).unwrap();
		header_json
	}

	fn as_b64url(&self) -> String {
		let header_json = self.as_json();
		let header_b = header_json.as_str().as_bytes();
		let header_base64 = BASE64URL.encode(header_b);
		header_base64
	}
}

#[derive(Deserialize, Serialize, Debug)]
struct Jwt {
	header: JwtHeader,
	claims: JwtClaims,
	signature: Vec<u8>,
}

impl Jwt {
	fn new() -> Jwt {
		// Get the private key from file
		// Find the keys file
		let path = format!("{}/keys.json", env!("CARGO_MANIFEST_DIR"));
		// Read it
		let contents = std::fs::read_to_string(path).expect("Could not open Keys file");
		// Parse the json to a struct
		let keys_file: KeysFile = serde_json::from_str(contents.as_str()).unwrap();
		// Get the private key from the struct
		let pkey_from_file = keys_file.private_key;
		//println!("private key from file: {}", pkey_from_file);
		// Convert it to a byte array
		let pkey_from_file = pkey_from_file.as_bytes();
		// Format it to openssl PKey Struct
		let pkey = PKey::private_key_from_pem(pkey_from_file).unwrap();

		// Sign data
		// Prepare signer
		let mut signer = Signer::new(MessageDigest::sha256(), &pkey).unwrap(); 

		// Get data to sign
		// Header struct
		let header =JwtHeader::new();
		// Claims struct
		let claims = JwtClaims::new();
		// Header as base64url encoding
		let header_b64 = header.as_b64url();
		// Claims as base64url encoding
		let claims_b64 = claims.as_b64url();
		// Both header and claims as proper input to then sign
		let signing = format!("{}.{}", header_b64, claims_b64);
		//println!("Forming Signature: {}\n", signing);
		// Make this string into a byte array
		let signing = signing.as_bytes();

		// Sign it!
		signer.update(signing).unwrap();

		// Sigh... make it into a string agin
		let signature = signer.sign_to_vec().unwrap();

		// Fully formed JWT
		Jwt{header, claims, signature}
	}

	fn as_b64url(&self) -> String {
		// let signature_b = self.signature.as_bytes();
		let signature_b64 = BASE64URL.encode(self.signature.as_slice());

		format!("{}.{}.{}", self.header.as_b64url(), self.claims.as_b64url(), signature_b64)
	}
}

#[derive(Deserialize, Debug)]
struct OAuthTokenPayload {
	access_token: String,
	expires_in: u64,
	token_type: String,
}

#[derive(Deserialize, Serialize, Debug)]
struct SheetWritePayload {
	range: String,
	#[allow(non_snake_case)]
	majorDimension: String,
	values: Vec<Vec<String>>,
}

impl SheetWritePayload {
	fn new() -> SheetWritePayload {
		SheetWritePayload{
			range: String::from("Sheet1!A1:B1"),
			majorDimension: String::from("ROWS"),
			values: Vec::from(Vec::new()),
		}
	}
}


//-----------------------------------------------
// Write to Google Sheets

pub async fn get_access_token() -> String {
	// Get and access token

	// Prepare a JWT Payload
	let jwt = Jwt::new();
	// Create the base64url encoded string
	let jwt_b64 = jwt.as_b64url();
	// println!("assertion: {}", jwt_b64);
	// Create a json data payload
	let json_payload = format!(r#"
{{
	"grant_type":"urn:ietf:params:oauth:grant-type:jwt-bearer",
	"assertion": "{}"
}}
	"#, jwt_b64);

	// Create a hyper based HTTPS client
	let https = HttpsConnector::new();
	let client = Client::builder().build::<_, hyper::Body>(https);

	// Form a POST request
	let req = Request::builder()
		.method(Method::POST)
		.uri("https://oauth2.googleapis.com/token")
		.body(Body::from(json_payload))
		.expect("Error with POST request builder");

	let resp = client.request(req).await.unwrap();
	let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();

	let oauth_resp: OAuthTokenPayload = serde_json::from_slice(&body).unwrap();
	let access_token = oauth_resp.access_token;
	let access_token = str::replace(access_token.as_str(), "........................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................................", "");
	access_token
}

pub async fn write_gsheet_write_asset_values(sheet_id: &String, sheet_name: &String, range: &Range, assets_and_values: &Vec<(Option<String>, Option<f64>)>) {
	// Get access token
	let access_token = get_access_token().await;

	// Create the payload to write to the Google Sheet
	let mut payload = SheetWritePayload::new();
	payload.range = format!("{}!{}", sheet_name, range.clone().as_string());
	for asset_and_value in assets_and_values.iter() {
		let asset_name = asset_and_value.clone().0;
		let asset_value = asset_and_value.clone().1;
		let mut row: Vec<String> = Vec::new();
		match asset_name.clone() {
			Some(asset_name) => {
				row.push(asset_name);
			},
			None => {
				row.push(String::from(""));
			}
		}
		match asset_value {
			Some(asset_value) => {
				row.push(format!("{}", asset_value));
			},
			None => {
				match asset_name {
					// If there was an asset name but no value returned from search write 0 for no value
					Some(_asset_name) => {
						row.push(String::from("0"));
					},
					// If there was a blank spot for the asset name include blank value
					None => { 
						row.push(String::from(""));
					}
				}
			}
		}
		payload.values.push(row);
	}

	let writing = serde_json::to_string(&payload).unwrap();

	let https = HttpsConnector::new();
	let client = Client::builder().build::<_, hyper::Body>(https);

	let uri = format!("https://sheets.googleapis.com/v4/spreadsheets/{}/values/{}!{}?valueInputOption=USER_ENTERED&access_token={}",
		sheet_id,
		sheet_name,
		range.as_string(),
		access_token
	);
	//if debug_on() >= 3 { println!("PUT to: {}\n", uri); }
	
	let req = Request::builder()
		.method(Method::PUT)
		.uri(uri)
		.body(Body::from(writing))
		.expect("Error with PUT request builder");

	let resp = client.request(req).await.unwrap();
	let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
	let _resp_payload: Value = serde_json::from_slice(&body).unwrap();

	//if debug_on() >= 3 { println!("{}", _resp_payload); } 
}
