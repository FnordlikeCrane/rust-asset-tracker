// ~/read_google_sheets.rs
//-----------------------------------------------

use hyper::Client;
use hyper_tls::HttpsConnector;
use serde::Deserialize;
use crate::config::*;
use crate::util_google_sheets::*;


//-----------------------------------------------
// Google Sheets Structs

#[derive(Deserialize, Debug)]
pub struct SheetReadPayload {
	// majorDimension: String,
	// range: String,
	pub values: Vec<Vec<Option<String>>>,
}


//-----------------------------------------------
// Read Google Sheets to Payload

pub async fn read_gsheet_from_range(sheet_name: &String, input: &Range) -> SheetReadPayload {
	let path = format!("{}/config.conf", env!("CARGO_MANIFEST_DIR"));
	let contents = std::fs::read_to_string(path).expect("Could not open Config file");
	let config: Config = toml::from_str(contents.as_str()).unwrap();
	let sheets_conf: SheetsConf = config.sheets;
	let credentials: CredentialsConf = config.credentials;
	
	let url = format!("https://sheets.googleapis.com/v4/spreadsheets/{}/values/{}!{}?key={}",
		sheets_conf.spreadsheet_id,
		sheet_name,
		input.as_string(),
		credentials.api_key
	);

	let uri = url.as_str().parse().unwrap();

	let https = HttpsConnector::new();
	let client = Client::builder().build::<_, hyper::Body>(https);	
	let resp = client.get(uri).await.unwrap();
	let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();

	let json_string = String::from_utf8_lossy(&body);
	let payload: SheetReadPayload = serde_json::from_str(&json_string).unwrap();
	payload
}


//-----------------------------------------------
// Read asset values

pub async fn read_gsheet_wanted_assets(sheet_id: &String, sheet_name: &String, range: &Range) -> Vec<Option<String>> {
	// First get the name of the sheet that stores assets so we can read that page
	let config = Config::default();
	let credentials: CredentialsConf = config.credentials;

	let url = format!("https://sheets.googleapis.com/v4/spreadsheets/{}/values/{}!{}?key={}",
		sheet_id,
		sheet_name,
		range.as_string(),
		credentials.api_key
	);

	let uri = url.as_str().parse().unwrap();

	let https = HttpsConnector::new();
	let client = Client::builder().build::<_, hyper::Body>(https);	
	let resp = client.get(uri).await.unwrap();
	let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();

	let json_string = String::from_utf8_lossy(&body);
	let payload: SheetReadPayload = serde_json::from_str(&json_string).unwrap();

	// println!("Parsing Data from GSheet:");
	let mut asset_names: Vec<Option<String>> = Vec::new();
	for row in payload.values.iter().skip(1) {
		// print!("Row parsed: {:?}", row);
		if row.len() >= 1 {
			let col1 = row[0].clone();
			// print!(", Row has adding to list: {:?}", col1);
			asset_names.push(col1);
		} else {
			// print!(", Row is Empty.");
			asset_names.push(None)
		}
		// print!("\n");
	}

	asset_names
}
