// ~/read_spreadsheet.rs
//-----------------------------------------------

use calamine::{open_workbook, Xlsx, Reader, RangeDeserializerBuilder};

use crate::spreadsheet_types::*;


//-----------------------------------------------
// Spreadsheet Functions

pub fn get_data_from_rows(path: &String, page_name: &String) -> (Vec<String>, Vec<ReadDataRow>) {
	let mut input_names: Vec<String> = Vec::new();
	let mut data: Vec<ReadDataRow> = Vec::new();

	let mut read_workbook: Xlsx<_> = open_workbook(path).expect("Cannot open file");
	let range = read_workbook.worksheet_range(page_name.as_str()).expect(format!("Unable to find page with name \"{}\" in file \"{}\"", page_name, path).as_str());
	let range = match range {
		Ok(range) => range,
		Err(e) => {
			println!("{}", e);
			println!("ERROR: Could not get data range from file \"{}\" with sheet name \"{}\".", path, page_name);
			return (input_names, data);
		},
	};

	let mut rows = RangeDeserializerBuilder::new().from_range(&range).unwrap();
	loop {
		if let Some(row) = rows.next() {
			let (symbol, asset, asset_class, api_source, desired_data): (String, String, String, String, String) = row.unwrap();
			let row = ReadDataRow::from_raw_data(&symbol, &asset, &asset_class, &api_source, &desired_data);
			input_names.push(symbol);
			data.push(row);
		} else {
			break;
		}
	}

	(input_names, data)
}
